from django.db import models

class PizzaShop(models.Model):
    name = models.CharField(max_length=30, verbose_name='Пицеррия')
    description = models.TextField(verbose_name='Описание')
    rating = models.FloatField(default=0, verbose_name='Рейтинг')
    url = models.URLField(verbose_name='Интернет-адресс пиццерии')

    class Meta:
        verbose_name = 'Пицеррия'
        verbose_name_plural ='Пицеррии'

    def __str__(self):
        return self.name

class Pizza(models.Model):
    pizzashop = models.ForeignKey(PizzaShop, on_delete=models.CASCADE)
    name = models.CharField(max_length=30,verbose_name='Название пиццы')
    short_description = models.CharField(max_length=30,verbose_name='Краткое описание пиццы')
    price = models.IntegerField(default=0, verbose_name='Цена пиццы')
    photo = models.ImageField('Фото', upload_to='mysite/photos', default='')

    class Meta:
        verbose_name = 'Пицца'
        verbose_name_plural ='Пиццы'
        ordering = ['name']

    def __str__(self):
        return self.name

class Order(models.Model):
    pizza = models.ForeignKey(Pizza, verbose_name='Пицца', on_delete=models.CASCADE)
    name = models.CharField(max_length=30, verbose_name='Имя заказчика')
    phone = models.CharField(max_length=30, verbose_name='Телефон')
    date = models.DateTimeField(auto_now_add=True, verbose_name='Дата')





