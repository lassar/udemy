from django.urls import path
from testurlapp import views

urlpatterns = [
    # path('', views.home, name='home'),
    # path('user/<int:month>/', views.home, name='home'),
    path('user/<int:month>/<int:year>/', views.home, name='home'),
    # http://127.0.0.1:8000/test_app/user/12/
]
